﻿using System;
using LevelObjects;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Editor
    [SerializeField] private float _upVelocity;
    [SerializeField] private float _initialForcePerTick;
    [SerializeField] private float _decreaseForcePerTick;
    [SerializeField] private float _angularPerTick;
    [SerializeField] private float _speedPerBonus;
    [SerializeField] private float _maxOffset;
    [SerializeField] private float _playerXMoveSpeed;
    [SerializeField] private Rigidbody _playerRB;
    [SerializeField] private Transform _ragdollCenter;

    public bool Started { get; private set; }

    //Private
    private float _currentTickZForce, _initialX;

    //Private Static
    private static PlayerController _instance;
    private static readonly Vector3 UP_VECTOR = new Vector3(0, 0.4f, 0.3f);

    private void Awake()
    {
        _instance = this;
        _initialX = _ragdollCenter.position.x;
        GameEventManager.Get().GameStart += OnGameStart;
    }

    public static PlayerController Get()
        => _instance;

    public void AddUpForce()
        => _playerRB.AddForce(UP_VECTOR * _currentTickZForce * 5f);

    public void AddForceBonus(bool goodBonus)
    {
        var bonusSpeed = goodBonus ? _speedPerBonus : -_speedPerBonus;
        _currentTickZForce += bonusSpeed;
    }

    void OnGameStart()
    {
        Started = true;
        _currentTickZForce = _initialForcePerTick;
    }

    void OnTickForce()
    {
        var vel = _playerRB.velocity;
        vel.z = _currentTickZForce * Time.fixedDeltaTime;
        _playerRB.velocity = vel;
        _playerRB.angularVelocity = Vector3.forward * _angularPerTick * Time.fixedDeltaTime;
        _currentTickZForce = Mathf.Clamp(_currentTickZForce - _decreaseForcePerTick * Time.fixedDeltaTime, 0, 9999);
    }

    void OnPlayerControl()
    {
        var delta = InputController.Get().Delta;
        if (delta < 0 && _ragdollCenter.position.x <= _initialX - _maxOffset) return;
        if (delta > 0 && _ragdollCenter.position.x >= _initialX + _maxOffset) return;
        transform.position += new Vector3(delta * _playerXMoveSpeed * Time.fixedDeltaTime, 0, 0);
    }

    private void FixedUpdate()
    {
        if (!Started) return;
        OnTickForce();
        OnPlayerControl();
    }

    private void OnDestroy()
    {
        GameEventManager.Get().GameStart -= OnGameStart;
    }
}