﻿using System;
using LevelObjects;
using UnityEngine;

public class BodyGroundTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var player = PlayerController.Get();
        if (!player.Started || !Ground.isGround(other.gameObject)) return;
        player.AddUpForce();
    }
}