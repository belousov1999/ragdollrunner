﻿using UnityEngine;

namespace LevelObjects
{
    public class FinishTrigger : MonoBehaviour
    {
        private bool _finished;

        private void OnTriggerEnter(Collider other)
        {
            if (_finished) return;
            _finished = true;
            var allyPlayer = PlayerController.Get();
            if (allyPlayer.gameObject == other.gameObject)
                GameEventManager.Get().SetWin();
        }
    }
}