﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LevelObjects
{
    public class Ground:MonoBehaviour
    {
        private static readonly List<GameObject> _grounds = new List<GameObject>();

        private void Awake()
        {
            _grounds.Add(this.gameObject);
        }

        public static bool isGround(GameObject gameObject)
            => _grounds.Contains(gameObject);
        
        private void OnDestroy()
        {
            _grounds.Remove(this.gameObject);
        }

    }
}