﻿using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //Editor
    [SerializeField] private Transform mainTarget;

    //Private
    private Vector3 _cameraOffset;
    private Tweener _moveTweener;

    private void Awake()
    {
        _cameraOffset = mainTarget.position - transform.position;
        _moveTweener = transform.DOMove(transform.position, 0.3f).SetEase(Ease.Linear).SetUpdate(UpdateType.Fixed).SetAutoKill(false);
    }

    void FixedUpdate()
    {
        var newPosition = mainTarget.position - _cameraOffset;
        newPosition.x = transform.position.x;
        _moveTweener.ChangeEndValue(newPosition, true).Restart();
    }
}
