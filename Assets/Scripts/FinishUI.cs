﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// End screen interface
/// </summary>
public class FinishUI : MonoBehaviour
{
    [SerializeField] private GameObject _gameWinUi;
    [SerializeField] private Button _nextButton;

    private void Awake()
    {
        _nextButton.onClick.AddListener(NextLevel);
        _gameWinUi.SetActive(false);

        GameEventManager.Get().GameWin += GameWin;
    }

    private void GameWin()
    {
        _gameWinUi.SetActive(true);
    }

    private void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnDestroy()
    {
        GameEventManager.Get().GameWin -= GameWin;
    }
}
