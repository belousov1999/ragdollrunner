﻿using System.Collections.Generic;
using UnityEngine;

public class MapBuilder : MonoBehaviour
{
    //Editor
    [SerializeField] private GameObject _emptyElement;
    [SerializeField] private GameObject _obstacleElement;
    [SerializeField] private GameObject _finishElement;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float _count;

    //Private
    private readonly List<GameObject> _elements = new List<GameObject>();
    private int indexElement;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        var first_el = Instantiate(_emptyElement, transform.position, Quaternion.identity);
        first_el.transform.SetParent(transform);
        _elements.Add(first_el);

        while (indexElement<_count) InstNextElement();

        var finish_el = Instantiate(_finishElement,
            _elements[_elements.Count - 1].transform.position + offset, Quaternion.identity);
        finish_el.transform.SetParent(transform);
        _elements.Add(finish_el);
    }

    private void InstNextElement()
    {
        InstantiateElement(_obstacleElement);
        indexElement++;
    }

    private void InstantiateElement(GameObject element)
    {
        var inst_el = Instantiate(element, _elements[_elements.Count - 1].transform.position + offset, Quaternion.identity);
        inst_el.transform.SetParent(transform);
        _elements.Add(inst_el);
    }
}