﻿using System;

public class GameEventManager
{
    public Action GameStart;
    public Action GameWin;

    //Private Static
    private static GameEventManager _instance;

    public static GameEventManager Get()
        => _instance ??= new GameEventManager();

    public void SetStart()
    {
        GameStart?.Invoke();
    }

    public void SetWin()
    {
        GameWin?.Invoke();
    }
}