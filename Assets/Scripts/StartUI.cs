﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class StartUI : MonoBehaviour
{
    private void ClickStart()
    {
        gameObject.SetActive(false);
        GameEventManager.Get().SetStart();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            ClickStart();
    }
}
