﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Random = UnityEngine.Random;

public class Bonus : MonoBehaviour
{
    [SerializeField] private bool _goodBonus;

    private void Awake()
    {
        var r = Random.Range(0, 100);
        gameObject.SetActive(r<=5);
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = PlayerController.Get();
        if (player.gameObject != other.transform.root.gameObject) return;
        player.AddForceBonus(_goodBonus);
        Destroy(gameObject);
    }
}
