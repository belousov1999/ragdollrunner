using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputController : MonoBehaviour, IDragHandler
{
    public float Delta { get; private set; }

    private static InputController _instance;
    
    private void Awake()
    {
        _instance = this;
    }

    public static InputController Get()
        => _instance;

    public void OnDrag(PointerEventData eventData)
    {
        Delta += eventData.delta.x * 0.1f;
    }

    private void FixedUpdate()
    {
        if (Delta == 0)
            return;
        Delta *= 0.5f;
    }
}
